## Changelog

#### Prolice 0.2.4
* Fix distribution bug
* Enable support for ARM-USB-TINY-H

#### Prolice 0.2.2
* Fix PyPi distribution

#### Prolice 0.2.1
* Updated documentation

#### Prolice 0.2.0
* Initial release
